<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class Incident extends Model
{
    protected $fillable = [
        'beneficiarId', 'situationId', 'messageForDoner','status','lat','lng','address','type'
    ];
    public function photo(){
        return $this->hasMany('App\IncidentImg','incidentId');
    }
    
    public function donation(){
        $user = JWTAuth::parseToken()->authenticate();
        return $this->hasMany('App\Donation','incidentId')->where([['donerId',$user->id],['isCancel',0]]);
    }
    public function moneyDonation(){
        $user = JWTAuth::parseToken()->authenticate();
        return $this->hasMany('App\MoneyForDonation','incidentId')->where('donerId',$user->id);
    }
    public function donationCancel(){
        return $this->hasMany('App\DonationCancel','donationId');
    }
   
    public function beneficiar(){
        return $this->belongsTo('App\User','beneficiarId');
    }
    public function situation(){
        return $this->belongsTo('App\M_situation','situationId');
    }
}

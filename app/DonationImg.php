<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationImg extends Model
{
    protected $fillable = [
        'donationId', 'imgUrl'
    ];
}

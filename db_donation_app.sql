-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2020 at 08:47 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_donation_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `donations`
--

CREATE TABLE `donations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pickupDate` datetime NOT NULL,
  `pickupTime` datetime NOT NULL,
  `incidentId` int(11) NOT NULL,
  `riderId` int(11) DEFAULT NULL,
  `donerId` int(11) NOT NULL,
  `lat` decimal(8,2) NOT NULL,
  `lng` decimal(8,2) NOT NULL,
  `shareMessage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `isCancel` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donations`
--

INSERT INTO `donations` (`id`, `pickupDate`, `pickupTime`, `incidentId`, `riderId`, `donerId`, `lat`, `lng`, `shareMessage`, `status`, `isCancel`, `created_at`, `updated_at`) VALUES
(1, '9999-12-31 23:59:59', '9999-12-31 23:59:59', 2, 1, 3, '2.00', '2.00', 'dfvdfv', '', 0, '2020-01-23 04:08:20', '2020-01-23 04:08:20'),
(2, '9999-12-31 23:59:59', '9999-12-31 23:59:59', 1, 1, 3, '2.00', '2.00', 'dfvdfv', '', 0, '2020-01-23 04:15:57', '2020-01-23 04:15:57'),
(3, '9999-12-31 23:59:59', '9999-12-31 23:59:59', 3, 1, 3, '2.00', '2.00', 'dfvdfv', '', 0, '2020-01-23 04:08:20', '2020-01-23 04:08:20');

-- --------------------------------------------------------

--
-- Table structure for table `donation_cancels`
--

CREATE TABLE `donation_cancels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `donerId` int(11) NOT NULL,
  `incidentId` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donation_cancels`
--

INSERT INTO `donation_cancels` (`id`, `donerId`, `incidentId`, `reason`, `created_at`, `updated_at`) VALUES
(4, 3, 1, 'dfjsjdfh', NULL, NULL),
(6, 4, 1, 'something', '2020-01-26 07:13:49', '2020-01-26 07:13:49');

-- --------------------------------------------------------

--
-- Table structure for table `donation_imgs`
--

CREATE TABLE `donation_imgs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `donationId` int(11) NOT NULL,
  `imgUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donation_imgs`
--

INSERT INTO `donation_imgs` (`id`, `donationId`, `imgUrl`, `created_at`, `updated_at`) VALUES
(1, 1, 'fddfbcbcv', NULL, NULL),
(2, 1, 'hhhhhhhhhhh', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `incidents`
--

CREATE TABLE `incidents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `beneficiarId` int(11) NOT NULL,
  `situation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `messageForDoner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` decimal(8,2) NOT NULL,
  `lng` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `incidents`
--

INSERT INTO `incidents` (`id`, `beneficiarId`, `situation`, `messageForDoner`, `status`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, 2, 'earthquake', 'Please help', 'pending', '2.00', '2.00', '2020-01-23 01:38:17', '2020-01-23 01:38:17'),
(2, 2, 'earthquake', 'Please help', 'pending', '2.00', '2.00', '2020-01-23 01:56:36', '2020-01-23 01:56:36'),
(3, 2, 'earthquake', 'Please help', 'pending', '2.00', '2.00', '2020-01-23 01:57:23', '2020-01-23 01:57:23'),
(4, 6, 'earthquake', 'Please help', 'pending', '2.00', '2.00', '2020-01-27 00:48:09', '2020-01-27 00:48:09'),
(5, 8, 'earthquake', 'Please help', 'pending', '2.00', '2.00', '2020-01-28 01:23:51', '2020-01-28 01:23:51');

-- --------------------------------------------------------

--
-- Table structure for table `incident_imgs`
--

CREATE TABLE `incident_imgs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incidentId` int(11) NOT NULL,
  `imgUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_22_113519_create_incidents_table', 1),
(5, '2020_01_22_115423_create_incident_imgs_table', 1),
(6, '2020_01_22_115500_create_donations_table', 1),
(7, '2020_01_22_115528_create_donation_imgs_table', 1),
(8, '2020_01_22_115551_create_donation_cancels_table', 1),
(9, '2020_01_22_115656_create_rider_details_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rider_details`
--

CREATE TABLE `rider_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `donationId` int(11) NOT NULL,
  `riderId` int(11) NOT NULL,
  `pickupDate` date NOT NULL,
  `pickupTime` time NOT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rider_details`
--

INSERT INTO `rider_details` (`id`, `donationId`, `riderId`, `pickupDate`, `pickupTime`, `details`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-11-11', '06:12:46', 'dszgsdfhdgfhdf', 'sdfgsfdhdfh', '2020-01-26 00:15:57', '2020-01-26 00:15:57'),
(2, 1, 1, '2020-11-11', '06:12:46', 'dszgsdfhdgfhdf', 'sdfgsfdhdfh', '2020-01-26 00:19:02', '2020-01-26 00:19:02'),
(3, 1, 1, '2020-11-11', '06:12:46', 'dszgsdfhdgfhdf', 'sdfgsfdhdfh', '2020-01-26 00:19:14', '2020-01-26 00:19:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobileNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` decimal(8,2) NOT NULL,
  `lng` decimal(8,2) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `userType`, `mobileNumber`, `image`, `lat`, `lng`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Test Man', 'test@email.com', NULL, '$2y$10$LxOg0jBAc0eBDJNUKxIXUexCd0Q901yKCMn91Sm7EiW3EEJjTAS2a', 'Rider', '12345678', 'reterwt', '23.00', '11.00', NULL, '2020-01-22 23:56:39', '2020-01-22 23:56:39'),
(2, 'benificiar', 'benificiar@email.com', NULL, '$2y$10$w4mIluEBk3G2mYCNpEcCI.IUy6p43lmLq4Yrw4oKAeI8oXhYej7CW', 'Benificiar', '12345678', 'reterwt', '23.00', '11.00', NULL, '2020-01-22 23:57:36', '2020-01-22 23:57:36'),
(3, 'doner', 'doner@email.com', NULL, '123456', 'Doner', '12345678', 'reterwt', '23.00', '11.00', NULL, '2020-01-22 23:58:04', '2020-01-22 23:58:04'),
(4, 'dsdd', 'doner1@email.com', NULL, '$2y$10$H7dnKYL69YJIE8GmNUmMPeM7TFnQExxwYhDrFVw350tikPnQJJpxy', 'Doner', '12345678', 'dsdd', '44.00', '44.00', NULL, '2020-01-26 07:06:55', '2020-01-26 07:06:55'),
(5, 'aa', 'doner2@email.com', NULL, '$2y$10$15ZEgEVZgKPpDB2/94osie9Ldcf.ryuNb6MaGKUIP01zDAwMwSYM2', 'Doner', '12345678', 'dsdd', '33.00', '33.00', NULL, '2020-01-26 23:34:02', '2020-01-26 23:34:02'),
(6, 'Benificiar', 'Benificiar12@email.com', NULL, '$2y$10$1BAtbfzTzdSW8XAjKAblxONgag5R/8F0LzWyP13QIqXaq56EsGf9K', 'Benificiar', '12345678', 'dsdd', '33.00', '33.00', NULL, '2020-01-27 00:23:31', '2020-01-27 00:23:31'),
(8, 'Benificiar', 'Benificiar56@email.com', NULL, '$2y$10$nRIhtedYUBr2JUqPK72RLu5Fxv/KJIomBY2TfudX.tnAFqTARIC8.', 'Benificiar', '12345678', 'dsdd', '33.00', '33.00', NULL, '2020-01-28 01:19:33', '2020-01-28 01:19:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `donations`
--
ALTER TABLE `donations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donation_cancels`
--
ALTER TABLE `donation_cancels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donation_imgs`
--
ALTER TABLE `donation_imgs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incident_imgs`
--
ALTER TABLE `incident_imgs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `rider_details`
--
ALTER TABLE `rider_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `donations`
--
ALTER TABLE `donations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `donation_cancels`
--
ALTER TABLE `donation_cancels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `donation_imgs`
--
ALTER TABLE `donation_imgs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `incidents`
--
ALTER TABLE `incidents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `incident_imgs`
--
ALTER TABLE `incident_imgs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `rider_details`
--
ALTER TABLE `rider_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

namespace App\Http\Controllers;
use Auth;
use App\Incident;
use App\RiderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\User;
use App\Donation;
use Tymon\JWTAuth\Exceptions\JWTException;

class RiderdetailController extends Controller
{
    public function addRiderDetail(Request $request){
        $data = $request->all();
        // if(!User::checkToken($request)){
        //     return response()->json([
        //      'message' => 'Token is required'
        //     ],422);
        // }

        //  $user = JWTAuth::parseToken()->authenticate();
        //  $data['riderId']= $user->id;
        // if(User::checkToken($request)){
        //     $user = JWTAuth::parseToken()->authenticate();
        //     $data['riderId']= $user->id; 
        // }
        // if ($data['imgUrl'] == null) {
        //     $data['imgUrl'] = `/uploads/images.jpg`;
        // }
        // $photolink = $data['imgUrl'];
        // unset($data['imgUrl']);
        $RiderDetail = RiderDetail::create($data);

        // $photo = [];
        // $ob1 = [];
        // $ob1['incidentId'] = $incident->id;
        // $ob1['imgUrl'] = $photolink;
        // array_push($photo,$ob1);
        
        // IncidentImg::insert($photo);
        return response()->json([
            'riderDetail' => $RiderDetail,
            'success' => true
        ],200);
    }
    public function addRiderforDonation(Request $request,$id){
        $data = $request->all();
        $donationId = $id;
        
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }

         $user = JWTAuth::parseToken()->authenticate();
         $data['riderId']= $user->id;
        // if(User::checkToken($request)){
        //     $user = JWTAuth::parseToken()->authenticate();
        //     $data['riderId']= $user->id; 
        // }
        // if ($data['imgUrl'] == null) {
        //     $data['imgUrl'] = `/uploads/images.jpg`;
        // }
        // $photolink = $data['imgUrl'];
        // unset($data['imgUrl']);
        $ob =[
            'riderId' => $data['riderId'],
            'status' => $data['status']
        ];
        $RiderforDonation = Donation::where([['id', $donationId],['riderId',NULL]])->update($ob);
        if(!$RiderforDonation){
            return response()->json([
                'success' => false,
                'message' => 'Item has been taken already by other Rider',
            ], 200);
        }
        
       else{
            return response()->json([
                'riderforDonation' => $RiderforDonation,
                'message' => 'Item is now assigned to you',
                'success' => true
            ],200);
        }
    }
    public function riderAddImageforDonation(Request $request,$id){
        $data = $request->all();
        $donationId = $id;
        
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }

        $user = JWTAuth::parseToken()->authenticate();
        $riderId= $user->id;
        $ob =[
            'status' => $data['status'],
            'image' => $data['image']
        ];
        $RiderTakeDonation = Donation::where([['id', $donationId],['riderId',$riderId]])->update($ob);
        if(!$RiderTakeDonation){
            return response()->json([
                'success' => false,
                'message' => 'Item has been taken already by other Rider',
            ], 200);
        }
        else{
                return response()->json([
                    'riderTakeDonation' => $RiderTakeDonation,
                    'message' => 'Item is now assigned to you',
                    'success' => true
                ],200);
            }
    }

    public function upload(Request $request){
        // \Log::info($request->all());
            
            request()->file('image')->store('uploads');
            $pic= $request->image->hashName();
            $pic= "/uploads/$pic";
            /*update the profile pic*/
            //return Gallery::create($data);
            return response()->json([
                'image'=> $pic
            ],200);
    }
    // public function showSingleIncident(Request $request,$id){
    //     $data = $request->all();
    //     if(User::checkToken($request)){
    //         $user = JWTAuth::parseToken()->authenticate();
    //         $data['beneficiarId']= $user->id; 
    //     }
    //     $incident = Incident::where('id',$id)->with('photo')->get();
    //     return response()->json([
    //         'incident' => $incident,
    //         'success' => true
    //     ],200);
    // }
    // public function deleteIncident($id)
    // {
    //     $incident = Incident::where('id','=',$id)
    //       ->first();
    //       if($incident->count()){
    //         $incident->delete();
    //         return response()->json(['msg'=>'success','status'=>$id]);
    //       } else {
    //         return response()->json(['msg'=>'error','status'=>$id]);
    //       }
    // }
}

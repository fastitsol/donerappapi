<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiderDetail extends Model
{
    protected $fillable = [
        'donationId', 'riderId','pickupDate','pickupTime','details','image'
    ];
    // public function rider(){
    //     return $this->belongsTo('App\Incident','incidentId');
    // }
    // public function donation(){
    //     return $this->belongsTo('App\Incident','incidentId');
    // }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationCancel extends Model
{
    protected $fillable = [
        'donerId','donationId', 'reason'
    ];
}

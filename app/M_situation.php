<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_situation extends Model
{
    protected $fillable =['situation'];
}

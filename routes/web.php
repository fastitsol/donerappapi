<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::post('/app/register', 'UserController@register');
//     Route::post('/app/login', 'UserController@authenticate');
//     Route::get('/app/open', 'DataController@open');

//     Route::group(['middleware' => ['jwt.verify']], function() {
//         Route::get('/app/user', 'UserController@getAuthenticatedUser');
//         Route::get('/app/closed', 'DataController@closed');
//     });
// Route::post('/app/userUpload', "UserController@upload");
// Route::post('/app/incidentUpload', "IncidentController@upload");





Route::post('/app/register', 'UserController@register');
Route::post('/app/login', 'UserController@authenticate');
Route::post('/app/profileEdit', 'UserController@profileEdit');
Route::get('/app/open', 'DataController@open');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('/app/user', 'UserController@getAuthenticatedUser');
    Route::get('/app/closed', 'DataController@closed');
});
Route::post('/app/userUpload', "IncidentController@upload");
Route::post('/app/incidentUpload', "IncidentController@upload");
Route::post('/app/ReporterVideoUpload', "IncidentController@videoUpload");
Route::post('/app/RiderUploadImg', "IncidentController@upload");
Route::post('/app/addIncident', "IncidentController@addIncident");
Route::post('/app/addPublicReport', "IncidentController@addPublicReport");
Route::post('/app/pushNotification', "IncidentController@pushNotification");
Route::get('/app/allUnseenNotification','IncidentController@allUnseenNotification');
Route::get('/app/allNotification','IncidentController@allNotification');
Route::post('/app/allNotifUpdateToSeen','IncidentController@allNotifUpdateToSeen');
Route::post('/app/specificNotifUpdateToSeen','IncidentController@specificNotifUpdateToSeen');
Route::get('/app/showSingleIncident/{id}','IncidentController@showSingleIncident');

Route::get('/app/showSituation','IncidentController@showSituation');
Route::get('/app/showIncident','IncidentController@showIncident');
Route::get('/app/showAppLink','IncidentController@showAppLink');
Route::get('/app/showEmergency','IncidentController@showEmergency');
Route::get('/app/showLatestIncident','IncidentController@showLatestIncident');
Route::get('/app/showLatestPublicReport','IncidentController@showLatestPublicReport');
// donation start
Route::get('/app/showIncidentforDoner','IncidentController@showIncidentforDoner');
//for giving thanks to doner
Route::post('/app/giveThanks', "IncidentController@giveThanks");
Route::get('/app/showThanks', "IncidentController@showThanks");

Route::post('/app/addDonation','DonationController@addDonation');
Route::post('/app/addDonationPaymentImg','DonationController@addDonationPaymentImg');
Route::post('/app/DonationImgUpload', "IncidentController@upload");
Route::get('/app/showDonation/{id}','DonationController@showDonation');
Route::get('/app/showDoner/{id}','IncidentController@showDoner');
Route::get('/app/showDonationforRider','DonationController@showDonationforRider');
// Route::post('/app/CancelationReasion/{id}','DonationController@CancelationReasion');
Route::post('/app/CancelDonation/{id}','DonationController@CancelDonation');
Route::post('/app/paymentImg/{id}','DonationController@paymentImg');
// Route::get('showDonationVieworEdit/{id}','IncidentController@showDonationVieworEdit');
// Route::get('showDonationVieworEdit/{id}','IncidentController@showDonationVieworEdit');
Route::post('/app/insidentStatusUpdate','IncidentController@insidentStatusUpdate');
Route::post('/app/donationStatusUpdate','IncidentController@donationStatusUpdate');


Route::post('/app/addRiderDetail','RiderdetailController@addRiderDetail');
Route::post('/app/addRiderforDonation/{id}','RiderdetailController@addRiderforDonation');
Route::post('/app/riderAddImageforDonation/{id}','RiderdetailController@riderAddImageforDonation');
Route::any('{slug}', 'UserController@home')->where('slug', '([0-9A-z_\-\s]+)?');



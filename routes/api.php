<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@authenticate');
    Route::get('open', 'DataController@open');

    Route::group(['middleware' => ['jwt.verify']], function() {
        Route::get('user', 'UserController@getAuthenticatedUser');
        Route::get('closed', 'DataController@closed');
    });
    Route::post('userUpload', "UserController@upload");
    Route::post('incidentUpload', "IncidentController@upload");
    Route::post('addIncident', "IncidentController@addIncident");
    Route::get('showSingleIncident/{id}','IncidentController@showSingleIncident');
    
    Route::get('showIncident','IncidentController@showIncident');
    Route::get('showIncidentforDoner','IncidentController@showIncidentforDoner');
    Route::post('CancleIncidentforDoner/{id}','IncidentController@CancleIncidentforDoner');


    Route::post('addDonation','DonationController@addDonation');
    Route::get('showDonation','DonationController@showDonation');
    Route::get('showDoner/{id}','IncidentController@showDoner');
    // Route::get('showDonationVieworEdit/{id}','IncidentController@showDonationVieworEdit');
    // Route::get('showDonationVieworEdit/{id}','IncidentController@showDonationVieworEdit');
    Route::post('insidentStatusUpdate','IncidentController@insidentStatusUpdate');
    Route::post('donationStatusUpdate','IncidentController@donationStatusUpdate');


    Route::post('addRiderDetail','RiderdetailController@addRiderDetail');
    Route::post('addRiderforDonation/{id}','RiderdetailController@addRiderforDonation');
    


<?php

    namespace App\Http\Controllers;

    use Auth;
    use App\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator;
    use JWTAuth;
    use Tymon\JWTAuth\Exceptions\JWTException;

    class UserController extends Controller
    {

        public function home(Request $request){
        
          
            return view('welcome');
    
        }
        public function profileEdit (Request $request){
            $data = $request->all();
            if(!User::checkToken($request)){
                return response()->json([
                'message' => 'Token is required'
                ],422);
            }
            $user = JWTAuth::parseToken()->authenticate();
            if(isset($data['token'])) unset($data['token']);
            $userprofileEdit = User::where('id',$user->id)->update($data);
            if($userprofileEdit){
                $c_user = User::where('id',$user->id)->first();
            }
            return response()->json([
                'user' => $c_user,
                'success' => true
            ],200);
        }
        public function authenticate(Request $request)
        {
            // $data = $request->all();
            $input = $request->only('device_id','password','appToken');
            $jwt_token = null;
            $tu = User::where('device_id',$input['device_id'])->count();
            if($tu==0){
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Device Id',
                ], 401);
            }
            $tuu = User::where([['device_id',$input['device_id']],['isActive',1]])->count();
            if($tu==0){
                return response()->json([
                    'success' => false,
                    'message' => 'Your account is Deactived by Admin',
                ], 401);
            }
            $d['device_id'] =  $input['device_id'];
            $d['password'] =  $input['password'];
            if (!$jwt_token = JWTAuth::attempt($d)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Device Id or Password',
                ], 401);
            }
            // $ob =[
            //     'appToken' => $input['appToken']
            // ];
            User::where('device_id',$input['device_id'])->update(['appToken' => $input['appToken']]);
            $user = Auth::user();
            return response()->json([
                'token' => $jwt_token,
                'user' => $user
            ],200);
        }
       

        public function register(Request $request)
        {
            // return  $request->all();
                $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                // 'userType' => 'required|string',
                'mobileNumber' => 'required|string|max:255',
                'device_id' => 'required|unique:users',
                'birthDate' => 'required',
                // 'lat' => 'required|string|max:255',
                // 'lng' => 'required|string|max:255',
                // 'image' => 'required',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6',
            ]);

            if($validator->fails()){
                return response()->json([
                    'success' => false,
                    'message' => 'Email or Device Id already been taken',
                ], 400);
            }

            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'device_id' => $request->get('device_id'),
                'mobileNumber' => $request->get('mobileNumber'),
                'lat' => $request->get('lat'),
                'lng' => $request->get('lng'),
                'image' => $request->get('image'),
                'birthDate' => $request->get('birthDate'),
                'paymaya' => $request->get('paymaya'),
                'gcash' => $request->get('gcash'),
                'password' => Hash::make($request->get('password')),
            ]);

            $token = JWTAuth::fromUser($user);

           return response()->json([
                'user' => $user,
                'token' => $token
             ],200);
        }

        public function getAuthenticatedUser()
            {
                    try {

                            if (! $user = JWTAuth::parseToken()->authenticate()) {
                                    return response()->json(['user_not_found'], 404);
                            }

                    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                            return response()->json(['token_expired'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                            return response()->json(['token_invalid'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                            return response()->json(['token_absent'], $e->getStatusCode());

                    }

                    return response()->json(compact('user'));
            }
    public function upload(Request $request)
        {
        // \Log::info($request->all());
            
            request()->file('image')->store('uploads');
            $pic= $request->image->hashName();
            $pic= "/uploads/$pic";
            /*update the profile pic*/
            //return Gallery::create($data);
            return response()->json([
                'image'=> $pic
            ],200);
        }
    }
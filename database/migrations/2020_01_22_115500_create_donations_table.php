<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('pickupDate');
            $table->datetime('pickupTime');
            $table->integer('incidentId');
            $table->integer('riderId');
            $table->integer('donerId');
            $table->decimal('lat');
            $table->decimal('lng');
            $table->string('shareMessage');
            $table->string('isCancel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donations');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportVideo extends Model
{
    protected $fillable = [
        'incidentId', 'videoUrl'
    ];
}

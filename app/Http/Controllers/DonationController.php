<?php

namespace App\Http\Controllers;

use App\User;
use App\Incident;
use App\Donation;
use App\DonationCancel;
use App\DonationImg;
use App\MoneyForDonation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class DonationController extends Controller
{
    public function addDonation(Request $request){
        $data = $request->all();
        if(User::checkToken($request)){
            $user = JWTAuth::parseToken()->authenticate();
            $data['donerId']= $user->id; 
        }
        $photolink = $data['imgUrl'];
        unset($data['imgUrl']);
        $donation = Donation::create($data);
        $photo = [];
        if(sizeof($photolink)>0){
            foreach ($photolink as  $value) {
                $ob1 = [];
                $ob1['donationId'] = $donation->id;
                $ob1['imgUrl'] = $value['imgUrl'];
                array_push($photo,$ob1);
            }
            DonationImg::insert($photo);
        }
        return response()->json([
            'donation' => $donation,
            'success' => true
        ],200);
            
        
        
        
    }
    public function addDonationPaymentImg(Request $request){
        $data = $request->all();
        if(User::checkToken($request)){
            $user = JWTAuth::parseToken()->authenticate();
            $data['donerId']= $user->id; 
        }
        // $photolink = $data['imgUrl'];
        // unset($data['imgUrl']);
        $donation = MoneyForDonation::create($data);
        // $photo = [];
        // if(sizeof($photolink)>0){
        //     foreach ($photolink as  $value) {
        //         $ob1 = [];
        //         $ob1['donationId'] = $donation->id;
        //         $ob1['imgUrl'] = $value['imgUrl'];
        //         array_push($photo,$ob1);
        //     }
        //     DonationImg::insert($photo);
        // }
        return response()->json([
            'donation' => $donation,
            'success' => true
        ],200);
            
        
        
        
    }
    // public function addDonation(Request $request){
    //     $data = $request->all();
    //     if(User::checkToken($request)){
    //         $user = JWTAuth::parseToken()->authenticate();
    //         $data['donerId']= $user->id; 
    //     }
    //     $photolink = $data['imgUrl'];
    //     unset($data['imgUrl']);
    //     $donation = Donation::where([
    //         ['donerId',$data['donerId']],
    //         ['incidentId',$data['incidentId']],
    //         ['isCancel',1],
    //     ])->first();
    //     // $donationWC = Donation::where([
    //     //     ['donerId',$data['donerId']],
    //     //     ['isCancel','Cancel'],
    //     //     ['incidentId',$data['incidentId']]])->first();
           
            
    //         if($donation){
    //             if($donation['isCancel' == 1]){
    //                 $donation['isCancel'] = 0;
    //                 $donation = Donation::update($donation);
    //                 $photo = [];
    //                 if(sizeof($photolink)>0){
    //                     foreach ($photolink as  $value) {
    //                         $ob1 = [];
    //                         $ob1['donationId'] = $donation->id;
    //                         $ob1['imgUrl'] = $value['imgUrl'];
    //                         array_push($photo,$ob1);
    //                     }
    //                     DonationImg::insert($photo);
    //                 }
    //                 return response()->json([
    //                     'donation' => $donation,
    //                     'success' => true
    //                 ],200);
    //             }
    //             else{
    //                 return response()->json([
    //                     'message' => "Donation Already Exists!",
    //                     'success' => false
    //                 ],400);
    //             }
                
    //         }
    //         else{
    //             $donation = Donation::create($data);
    //             $photo = [];
    //             if(sizeof($photolink)>0){
    //                 foreach ($photolink as  $value) {
    //                     $ob1 = [];
    //                     $ob1['donationId'] = $donation->id;
    //                     $ob1['imgUrl'] = $value['imgUrl'];
    //                     array_push($photo,$ob1);
    //                 }
    //                 DonationImg::insert($photo);
    //             }
    //             return response()->json([
    //                 'donation' => $donation,
    //                 'success' => true
    //             ],200);
    //         }
        
        
    //     // if($donation){
    //     //     return response()->json([
    //     //         'donation' => $donation,
    //     //         'success' => true
    //     //     ],200);
    //     // }
    //     // else{
    //     //     return response()->json([
    //     //         'message' => 'Donation Already added!',
    //     //         'success' => false
    //     //     ],401);
    //     // }
    //     // $photo = [];
    //     // $ob1 = [];
    //     // $ob1['incidentId'] = $incident->id;
    //     // $ob1['imgUrl'] = $photolink;
    //     // array_push($photo,$ob1);
        
    //     // IncidentImg::insert($photo);
        
    // }

    public function upload(Request $request){
        // \Log::info($request->all());
            
            request()->file('imgUrl')->store('uploads');
            $pic= $request->image->hashName();
            $pic= "/uploads/$pic";
            /*update the profile pic*/
            //return Gallery::create($data);
            return response()->json([
                'imgUrl'=> $pic
            ],200);
    }
    public function showDonation(Request $request,$id){
        $MoneyDonation = MoneyForDonation::where('incidentId',$id)->get();
        $Donation = Donation::where([['incidentId',$id],['isCancel',0]])->with('rider','doner','photo')->get();
        return response()->json([
            'moneyDonation' => $MoneyDonation,
            'donations' => $Donation,
            'success' => true
        ],200); 
    }

    public function showDonationforRider(Request $request){
        $data = $request->all();
        $date = $request->date;
        $status = $request->status;
        // $Donation = Donation::where('incidentId',$id)->with('incident','photo')->whereDate('created_at',$date);
        
        $Donation = Donation::where('isCancel',0)->with('incident','photo','rider','doner')->whereDate('created_at',$date);
        if($status){
            $Donation = Donation::where([['isCancel',0],['status',$status]])->with('incident','photo','rider','doner')->whereDate('created_at',$date);
        }
        // $Donation->with('incident','photo')->get();
        return response()->json([
            'donationforRider' => $Donation->get(),
            'success' => true
        ],200);
    }
    public function CancelationReasion(Request $request , $id){
        
        $data = $request->all();
         $donationId = $id;
         if(User::checkToken($request)){
             $user = JWTAuth::parseToken()->authenticate();
             $data['donerId'] = $user->id; 
         }
         $data['donationId']=$donationId;
          $DonationCancel = DonationCancel::create($data);
         return response()->json([
             'donationCancel' => $DonationCancel,
             'success' => true
         ],200);
     }
    public function CancelDonation (Request $request ,$id){
        $data = $request->all();
        $donationId = $id;
        // $DonationCancel = Donation::where('id',$donationId)->delete();
        $DonationCancel = Donation::where('id',$donationId)->update([
            'isCancel'=>$data['isCancel'],
            'reason'=>$data['reason']
        ]);
        return response()->json([
            'donationCancel' => $DonationCancel,
            'success' => true
        ],200);
    }
    public function paymentImg (Request $request ,$id){
        $data = $request->all();
        $donationId = $id;
        // $DonationCancel = Donation::where('id',$donationId)->delete();
        $paymentImg = Donation::where('id',$donationId)->create([
            'payment_img'=>$data['payment_img']
        ]);
        return response()->json([
            'paymentImg' => $paymentImg,
            'success' => true
        ],200);
    }
    // public function showDonation(Request $request,$id){
    //     $data = $request->all();
    //     // if(User::checkToken($request)){
    //     //     $user = JWTAuth::parseToken()->authenticate();
    //     //     $data['beneficiarId']= $user->id; 
    //     // }
    //     $incident = Incident::where('id',$id)->with('photo')->get();
    //     return response()->json([
    //         'incident' => $incident,
    //         'success' => true
    //     ],200);
    // }
    // public function deleteIncident($id)
    // {
    //     $incident = Incident::where('id','=',$id)
    //       ->first();
    //       if($incident->count()){
    //         $incident->delete();
    //         return response()->json(['msg'=>'success','status'=>$id]);
    //       } else {
    //         return response()->json(['msg'=>'error','status'=>$id]);
    //       }
    // }
}

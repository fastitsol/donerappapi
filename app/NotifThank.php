<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifThank extends Model
{
    protected $fillable = [
        'from_beneficier_id', 'to_doner_id','donationId','message'
    ];
    public function beneficiar(){
        return $this->belongsTo('App\User','from_beneficier_id');
    }
    public function doner(){
        return $this->belongsTo('App\User','to_doner_id');
    }
    public function donation(){
        return $this->belongsTo('App\Donation','donationId');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportImg extends Model
{
    protected $fillable = [
        'publicReportId', 'imgUrl'
    ];
}

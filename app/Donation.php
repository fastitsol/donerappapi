<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
// use JWTAuth;
// use Tymon\JWTAuth\Exceptions\JWTException;

class Donation extends Model
{
    protected $fillable = [
        'pickupDate', 'pickupTime', 'incidentId','riderId','donerId','lat','lng','shareMessage','isCancel','address','reason','status','payment_img'
    ];
    public function incident(){
        return $this->belongsTo('App\Incident','incidentId');
    }
    public function rider(){
        return $this->belongsTo('App\User','riderId');
    }
    
    // public function doner(){
    //     $user = JWTAuth::parseToken()->authenticate();
    //     return $this->belongsTo('App\User','donerId')->where('donerId',$user->id);
    // }
    
    public function doner(){
        return $this->belongsTo('App\User','donerId');
    }
    public function photo(){
        return $this->hasMany('App\DonationImg','donationId');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicReport extends Model
{
    protected $fillable = [
        'reporterId', 'situationId', 'messageForAuthorities','lat','lng','address'
    ];
    public function photo(){
        return $this->hasMany('App\ReportImg','publicReportId');
    }
    public function video(){
        return $this->hasMany('App\ReportVideo','publicReportId');
    }
    public function situation(){
        return $this->belongsTo('App\M_situation','situationId');
    }
}

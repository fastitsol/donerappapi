<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyForDonation extends Model
{
    protected $fillable = [
        'incidentId', 'donerId','payment_img'
    ];
}

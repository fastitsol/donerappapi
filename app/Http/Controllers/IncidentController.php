<?php

namespace App\Http\Controllers;

use App\User;
use App\Notif;
use App\Incident;
use App\PublicReport;
use App\ReportVideo;
use App\ReportImg;
use App\Emergency;
use App\IncidentImg;
use App\NotifThank;
use App\M_situation;
use App\Donation;
use App\AppLink;
use App\DonationCancel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Builder;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class IncidentController extends Controller
{
    // protected $url = "https://bayanihan.santamariabulacan.org";
    
    public function addPublicReport(Request $request){
        $data = $request->all();
        if(User::checkToken($request)){
            $user = JWTAuth::parseToken()->authenticate();
            $data['reporterId']= $user->id; 
        }
        
        $photolink = $data['imgUrl'];
        unset($data['imgUrl']);
        $videolink = $data['videoUrl'];
        unset($data['videoUrl']);
        $PublicReport = PublicReport::create($data);

        $photo = [];
        if(sizeof($photolink)>0){
            foreach ($photolink as  $value) {
                $ob1 = [];
                $ob1['publicReportId'] = $PublicReport->id;
                $ob1['imgUrl'] = $value['imgUrl'];
                array_push($photo,$ob1);
            }
            ReportImg::insert($photo);
        }
        $video = [];
        if(sizeof($videolink)>0){
            foreach ($videolink as  $value) {
                $ob1 = [];
                $ob1['publicReportId'] = $PublicReport->id;
                $ob1['videoUrl'] = $value['videoUrl'];
                array_push($video,$ob1);
            }
            ReportVideo::insert($video);
        }
        
        $publicReport = PublicReport::where('id',$PublicReport->id)->with('photo','video')->first();

        return response()->json([
            'publicReport' => $publicReport,
            'success' => true
        ],200);
    }
    public function addIncident(Request $request){
        $data = $request->all();
        if(User::checkToken($request)){
            $user = JWTAuth::parseToken()->authenticate();
            $data['beneficiarId']= $user->id; 
        }
        
        $photolink = $data['imgUrl'];
        unset($data['imgUrl']);
        $incident = Incident::create($data);

        $photo = [];
        if(sizeof($photolink)>0){
            foreach ($photolink as  $value) {
                $ob1 = [];
                $ob1['incidentId'] = $incident->id;
                $ob1['imgUrl'] = $value['imgUrl'];
                array_push($photo,$ob1);
            }
            IncidentImg::insert($photo);
        }
        
        $Incident = Incident::where('id',$incident->id)->with('photo')->first();

        return response()->json([
            'incident' => $Incident,
            'success' => true
        ],200);
    }

    public function giveThanks(Request $request){
        // return $request;
        $data = $request->all();
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $data['from_beneficier_id']= $user->id;
        
        $NotifThank = NotifThank::where([
        ['from_beneficier_id',$data['from_beneficier_id']],
        ['donationId',$data['donationId']],
        ['to_doner_id',$data['to_doner_id']]])->first();
        if($NotifThank){
            return response()->json([
                'message' => "Your thanks already given",
                'success' => false
            ],400);
        }
        else{
            $NotifThank = NotifThank::create($data);
     
            return response()->json([
                'notifThank' => $NotifThank,
                'success' => true
            ],200);
        }
        
    }

    public function pushNotification(Request $request){   
        $massNoti = [];
        $ids = [];
        $users = User::all();
        foreach($users as $user){
            // $ids = [];
            $ids[0] = $user->appToken;
        // push notification start
            if ($ids) {
                //firebase
                $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = array(
                    
                    'registration_ids' => $ids,
                    'data' => array(
                        'title' => $request->title,
                        "message" => $request->msg,
                        'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                    ),
                    'notification' => array(
                        'title' => $request->title,
                        "body" => $request->msg,
                        "sound" => true,
                        "badge" => 1,
                    ),
                    'time_to_live' => 6000,

                );
                $fields = json_encode($fields);

                $headers = array(
                    'Authorization: key=' . "AAAAwiLjLBg:APA91bHfn-OqNkEfqOLU_Xy1phTsbS2aITpZ0aYUbvSgdwOluvh3r2ExwfJP13ZDGPqBG2NHcsQPv57IpNG9U87KrWyhAEXKwHqb6H2KPA5ThTfKgE9ZRIGNueVx_c8pJoSN-p4rBtf0",
                //take this key from app developer or Bijoya
                    'Content-Type: application/json'
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                $result = curl_exec($ch);
                curl_close($ch);
            }
            array_push($massNoti, [
                'fromId' => 70,
                'toId' => $user->id,
                'title' => $request->title,
                'msg' => $request->msg,
                'seen' => 0
            ]);
        }
        Notif::insert($massNoti);
        return response()->json([
            'success' => true
        ],200);

    }
    public function upload(Request $request){
        // \Log::info($request->all());
            $url = $request->url;
            // $url = \config('imgurl.url');
            request()->file('imgUrl')->store('uploads');
            $pic= $request->imgUrl->hashName();
            $pics="$url/uploads/$pic";
            /*update the profile pic*/
            //return Gallery::create($data);
            return response()->json([
                'imgUrl'=> $pics
            ],200);
    }
    public function videoUpload(Request $request){
        // \Log::info($request->all());
        
            $url = $request->url;
            // $url = \config('imgurl.url');
            request()->file('videoUrl')->store('uploads');
            $pic= $request->videoUrl->hashName();
            $vdo= "$url/uploads/$pic";
            /*update the profile pic*/
            //return Gallery::create($data);
            return response()->json([
                'videoUrl'=> $vdo,
                'success' => true
            ],200);
    }
    public function showSingleIncident(Request $request,$id){
        $data = $request->all();
        if(User::checkToken($request)){
            $user = JWTAuth::parseToken()->authenticate();
            $data['beneficiarId']= $user->id; 
        }
        $incident = Incident::where('id',$id)->with('photo')->get();
        return response()->json([
            'incident' => $incident,
            'success' => true
        ],200);
    }
    public function showThanks(Request $request){
        $data = $request->all();
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        // $data['from_beneficier_id']= $user->id;
        $NotifThank = NotifThank::where('to_doner_id',$user->id)->with('beneficiar','doner','donation')->get();
        return response()->json([
            'showThank' => $NotifThank,
            'success' => true
        ],200);
    }
    // public function CancelDonation(Request $request,$id){
    //     $data = $request->all();
    //     if(User::checkToken($request)){
    //         $user = JWTAuth::parseToken()->authenticate();
    //         $data['beneficiarId']= $user->id; 
    //     }
    //     $incident = Incident::where('id',$id)->with('photo')->get();
    //     return response()->json([
    //         'incident' => $incident,
    //         'success' => true
    //     ],200);
    // }
    public function showIncidentforDoner(Request $request){
        $data = $request->all();
        $date = $request->date;
        $user ='';
        if(User::checkToken($request)){
            $user = JWTAuth::parseToken()->authenticate();
            // $data['donerId']= $user->id; 
        }
        // $id = $data['donerId'];
        $Incident = Incident::whereDate('created_at',$date)->with('situation','photo','moneyDonation','donation','donation.rider','beneficiar')->get();
        // $incident = Incident::with('photo','donation')->get();
        return response()->json([
            'incident' => $Incident,
            'success' => true
        ],200);
    }
    // public function showIncidentforDoner(Request $request){
    //     $data = $request->all();
    //     $date = $request->date;
        
    //     $user ='';
    //     if(User::checkToken($request)){
    //         $user = JWTAuth::parseToken()->authenticate();
    //         $data['donerId']= $user->id; 
    //     }
    //     // $users = User::whereHas('posts', function($q){
    //     //     $q->where('created_at', '>=', '2015-01-01 00:00:00');
    //     // })->get();

    //     // $qq = Incident::whereDate('created_at',$date);
    //     // $Incident = Incident::whereDate('created_at',$date)->whereDoesntHave('donation', function(Builder $q) use ($elem1,$elem2){
    //     //     $q->where([['donerId', '=',$elem1],['incidentId', '=', $elem2]]);
    //     // })->with('photo','donation','donation.rider','beneficiar')->get();
    //     $id = $data['donerId'];
    //     $Incident = Incident::whereDate('created_at',$date)->whereDoesntHave('donationCancel', function(Builder $q) use ($id){
    //         $q->where([['donerId', '=',$id]]);
    //     })->with('photo','donation','donation.rider','beneficiar')->get();



    //     // $incident = Incident::with('photo','donation')->get();
    //     return response()->json([
    //         'incident' => $Incident,
    //         'success' => true
    //     ],200);
    // }
    
    public function showSituation(Request $request){
        $data = $request->all();
        $M_situation = M_situation::all();
        return response()->json([
            'm_situation' => $M_situation,
            'success' => true
        ],200);
    }
    public function allNotification(Request $request){
        $data = $request->all();
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $allNotification = Notif::where('toId',$user->id)->orderBy('id', 'desc')->get();
        return response()->json([
            'allNotification' => $allNotification,
            'success' => true
        ],200);
    }
    public function allUnseenNotification(Request $request){
        $data = $request->all();
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $allUnseenNotification = Notif::where([['toId',$user->id],['seen',0]])->count();
        return response()->json([
            'allUnseenNotif' => $allUnseenNotification,
            'success' => true
        ],200);
    }
    public function allNotifUpdateToSeen(Request $request){
        $data = $request->all();
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        return Notif::where('toId',$user->id)->update(['seen' => 1]);
        
    }
    public function specificNotifUpdateToSeen(Request $request){
        $data = $request->all();
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        return Notif::where([['id', $data['id']],['toId',$user->id]])->update(['seen' => 1]);
        
    }
    public function showDoner(Request $request,$id){
        $data = $request->all();
        // $date = $request->date;
        // $Donation = Donation::where('incidentId',$id)->with('incident','photo')->whereDate('created_at',$date);
        
        $Donation = Donation::where('incidentId',$id)->with('incident','photo')->get();
        // $Donation->with('incident','photo')->get();
        return response()->json([
            'donation' => $Donation,
            'success' => true
        ],200);
    }
    public function showIncident(Request $request){
        $data = $request->all();
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $beneficiarId= $user->id;
        // $date = $request->date;
        // $Donation = Donation::where('incidentId',$id)->with('incident','photo')->whereDate('created_at',$date);
        
        $Incident = Incident::with('photo')->get();
        // $Donation->with('incident','photo')->get();
        return response()->json([
            'incident' => $Incident,
            'success' => true
        ],200);
    }
    public function showAppLink(Request $request){
        $data = $request->all();
        $AppLink = AppLink::all();
        $appLink = $AppLink->first();
        return response()->json([
            'appLink' => $appLink,
            'success' => true
        ],200);
    }
    public function showEmergency(Request $request){
        $data = $request->all();
        // if(!User::checkToken($request)){
        //     return response()->json([
        //      'message' => 'Token is required'
        //     ],422);
        // }
        // $user = JWTAuth::parseToken()->authenticate();
        // $beneficiarId= $user->id;        
        $Emergency = Emergency::all();
        // $Donation->with('incident','photo')->get();
        return response()->json([
            'emergency' => $Emergency,
            'success' => true
        ],200);
    }
    public function showLatestIncident(Request $request){
        $data = $request->all();
        $date = $request->date;
        // $Donation = Donation::where('incidentId',$id)->with('incident','photo')->whereDate('created_at',$date);
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $beneficiarId= $user->id;
        $Incident = Incident::where('beneficiarId',$beneficiarId)->with('situation','photo')->whereDate('created_at',$date)->orderBy('id', 'desc')->first();
        // $Donation->with('incident','photo')->get();
        return response()->json([
            'incident' => $Incident,
            'success' => true
        ],200);
    }
    public function showLatestPublicReport(Request $request){
        $data = $request->all();
        $date = $request->date;
        // $Donation = Donation::where('incidentId',$id)->with('incident','photo')->whereDate('created_at',$date);
        if(!User::checkToken($request)){
            return response()->json([
             'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $reporterId= $user->id;
        $PublicReport = PublicReport::where('reporterId',$reporterId)->with('situation','photo','video')->whereDate('created_at',$date)->orderBy('id', 'desc')->get();
        
        return response()->json([
            'publicReport' => $PublicReport,
            'success' => true
        ],200);
    }
    
    public function showDonationVieworEdit(Request $request,$id){
        $data = $request->all();
        // $date = $request->date;
        // $Donation = Donation::where('incidentId',$id)->with('incident','photo')->whereDate('created_at',$date);
        
        $Incident = Incident::with('photo')->get();
        // $Donation->with('incident','photo')->get();
        return response()->json([
            'incident' => $Incident,
            'success' => true
        ],200);
    }
    public function deleteIncident($id)
    {
        $incident = Incident::where('id','=',$id)
          ->first();
          if($incident->count()){
            $incident->delete();
            return response()->json(['msg'=>'success','status'=>$id]);
          } else {
            return response()->json(['msg'=>'error','status'=>$id]);
          }
    }
    public function insidentStatusUpdate(Request $request){
        $data = $request->all();
       return Incident::where('id', $data['id'])->update($data);
        
    }
    public function donationStatusUpdate(Request $request){
        $data = $request->all();
        return Donation::where('id', $data['id'])->update($data);
        
    }
}

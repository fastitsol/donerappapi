<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentImg extends Model
{
    protected $fillable = [
        'incidentId', 'imgUrl'
    ];
}

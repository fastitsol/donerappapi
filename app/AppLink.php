<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppLink extends Model
{
    protected $fillable = [
        'ios', 'android'
    ];
}
